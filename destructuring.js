// function numbers(...args) {
//   console.log(args); // [1, 2, 3]
// }

// numbers(1, 2, 3);

// let [a, b, ...rest] = [1, 2, 3, 4, 5];

// console.log(a); // 1
// console.log(b); // 2
// console.log(rest); // [3, 4, 5]

// let { firstName, ...rest } = {
//   firstName: "Nandhu",
//   lastName: "kumar",
//   age: 26,
// };

// console.log(firstName); // Nandhu
// console.log(rest); // Object {lastName: "kumar", age: 26}

// function numbers(a, b, ...rest) {
//     console.log(a);  // 1
//     console.log(b);  // 2
//     console.log(rest);  // [3, 4, 5]
//   }
//   numbers(1, 2, 3, 4, 5);

// function numbers(a, ...rest, b) {
//     console.log(a);
//     console.log(rest);
//     console.log(b);
//   }
//   numbers(1, 2, 3, 4, 5);

let person = {
  firstName: "Nandhu",
  lastName: "kumar",
  age: 26,
};

// let { firstName } = person;
// console.log(firstName);

// let { firstName, gender } = person;
// console.log(firstName);     // Nandhu
// console.log(gender);        // undefined

let { firstName: myName } = person;
console.log(myName);        // Nandhu
