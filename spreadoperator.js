// let arr1 = [2, 3];
// let arr2 = [1, ...arr1, 4];

// console.log(arr2);  // [1, 2, 3, 4]

// let arr1 = [2, 3];
// let arr2 = [...arr1];

// console.log(arr2);  // [2, 3]

// let arr1 = [2, 3];
// let arr2 = [4, 5];
// let arr3 = [...arr1, ...arr2];

// console.log(arr3); // [2, 3, 4, 5]

// let person = { name: "Nandy", age: 26 };
// let personDetails = { ...person, city: "Tamilnadu" };

// console.log(personDetails); // Object {name: "Nandy", age: 26, city: "Tamilnadu"}

// let person = { name: "Nandy", age: 26 };
// let personDetails = { ...person };

// console.log(personDetails); // Object {name: "Nandy", age: 26}

// let person = { name: "Nandy", age: 26 };
// let address = { city: "Tamilnadu", pincode: 500001 };
// let personDetails = { ...person, ...address };

// console.log(personDetails); // Object {name: "Nandy", age: 26, city: "Tamilnadu", pincode: 500001}

// function add(a, b, c) {
//   return a + b + c;
// }
// let numbers = [1, 2, 3, 4, 5];

// // console.log(add(...numbers)); // 6

// let str = "Hello";

// // console.log(...str); // H,e,l,l,o

// // console.log(Array.from("123"));

// const set = [...new Set(["foo", "bar", "baz", "foo"])];
// Array.from(set);
// // [ "foo", "bar", "baz" ]
// console.log(set);

// console.log(Array.from(set));

const str = "Nandy";

const newArr = [...str]; // ['k', 'i', 't', 't', 'y'];
console.log(newArr);
