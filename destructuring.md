## How to use Destructuring ?

### Destructuring -:

- The destructuring assignment syntax in JavaScript expression that allows you to unpack values from arrays or properties from objects and store them in separate variables.

**_Rest parameter_**

- using rest parameter, we can unpack multiple values into an array

        function numbers(...rest) {

            console.log(rest);  // [1, 2, 3]
        }

        numbers(1, 2, 3);

**_Destructuring arrays and objects with Rest Parameter Syntax_**

- Destructuring the values of arrays using rest parameter syntax

- If we don't know the number of values passed in the function, Such cases we can use rest operator to receive all the data into an array

> Example of Array using rest parameter

        let [a, b, ...rest] = [1, 2, 3, 4, 5];

        console.log(a);  // 1
        console.log(b);  // 2
        console.log(rest);  // [3, 4, 5]

> Example of Objects using rest parameter

        let { firstName, ...rest } = {
        firstName: "Nandhu",
        lastName: "kumar",
        age: 26,
        };

        console.log(firstName); // Nandhu

        console.log(rest); // Object {lastName: "kumar", age: 26}

**_The Rest parameter should be the last parameter._**

- While destructuring function using rest parameter, rest paramter should be the last parameter

- Otherwise, It will cause Uncaught SyntaxError

> Correct way of using Rest parameter

        function numbers(a, b, ...rest) {

            console.log(a);  // 1
            console.log(b);  // 2
            console.log(rest);  // [3, 4, 5]
        }

        numbers(1, 2, 3, 4, 5);

> Wrong way

        function numbers(a, ...rest, b) {

            console.log(a);
            console.log(rest);
            console.log(b);
        }

        numbers(1, 2, 3, 4, 5);  // Uncaught SyntaxError: Rest parameter must be last formal parameter

**Object Destructuring**

        let person = {
            firstName: "Nandhu",
            lastName: "kumar",
            age: 26,
        };

        let { firstName } = person;
        console.log(firstName);     // Nandhu

In above case -:

- where firstName is the object property. Here, we are assigning that variable to destructure the value of property from object

- In this case we can assign number of properties to destructure

- If the object has that property it will return the value of property, Otherwise it will return undefined

Example -:

        let person = {
        firstName: "Nandhu",
        lastName: "kumar",
        age: 26,
        };

        let { firstName, gender } = person;
        console.log(firstName);     // Nandhu
        console.log(gender);        // undefined

**_Destructuring objects using different variable names_**

- If you'd like to create variables of different names than the properties, then you can use the aliasing feature of object destructuring.

  Example -:

        let person = {
            firstName: "Nandhu",
            lastName: "kumar",
            age: 26,
        };

        let { firstName: myName } = person;
        console.log(myName);        // Nandhu

> **Reference and Resources**

- [dmitripavlutin](https://dmitripavlutin.com/javascript-object-destructuring/)

- [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

- [javascript.info](https://javascript.info/destructuring-assignment)
