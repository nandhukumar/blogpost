## Data Types -:

- JavaScript values are of two categories

  - Primitive Datatypes -:

        - Number
        - String
        - Boolean
        - Undefined
        - Null

    > In memory, a primitive type has a fixed size.

    > For example, A number takes up eight bytes of memory
    >
    > While a boolean value requires only one bit. The number type is the most powerful among the primitive kinds.
    >
    > If each JavaScript variable saves eight bytes of memory, it can hold any primitive value directly.

  - Non-Primitive Datatypes -:

        - Arrays
        - Objects

    > However, reference types are a different story.
    > For example,Objects do not have a defined size and can be of any length.
    >
    > Arrays are the same way: they can have any number of elements.
    >
    > The values of these types cannot be stored directly in the eight bytes of memory since they do not have a defined size.
    >
    > Instead of storing the value, the variable saves a reference to it.
    > This reference is usually in the form of a pointer or a memory address.

## Call by Value

    - When we call a function with a value of variables as a parameter. Such functions are referred to as call by value.

    - Value should be of Primitive Datatypes

**Features of Call by Values**

> Functions arguments are always passed by value (Primitive Datatypes).

> It copies a value of variable and passed to a function as a local variable.

> Both these variables occupies the seperate locations in memory.

> So, if changes made in the particular variable does not affect the other one

Examples -:

    let value = 20;

    let copiedValue = value;

    copiedValue = copiedValue + 10;

    console.log(copiedValue); // output => 20

    console.log(value); // output => 30

In the above case,

- we are just copying the value of the valriable(20) and storing it to another variable called copiedValue

- Then, We are adding value 10 to the copiedValue variable.

- But, It won't affect the value which we copied from.

Example 2 -:

    function updateValue(value) {

        value += 10;
        return value
    }

    let value = 20;

    let updatedValue = updateValue(value);

    console.log(value);        // output => 20
    console.log(updatedValue); // output => 30

In above case,

- Am passing variable of value to the function called updateValue.

- Then, adding value with 10 and returning the value.

- By consoling both the values it shows that, changes made to the value inside the function does not update the value outside.

## Call by Reference

    - When we call a function, Instead of passing the value of variables, We pass reference of variables(memory location of variables) as a parameter. Such functions are referred to as Call by Reference.

    - Assigned Value should be of Non-Primitive Datatypes

**Features of Call by Reference**

> Functions arguments are always passed by reference of objects (Non-Primitive Datatypes).

> When an object is stored in a variable and that variable is set equal to another variable.

> Both the variables occupy the same memory location.

> So, if changes made in the particular variable affect the other one.

Example 2 -:

    let arr = [1, 2, 3];

    function add_item(arr) {

        arr.push(4);
        console.log(arr);
    }

    console.log(arr); // output => [ 1, 2, 3]

    add_item(arr);      // output => [ 1, 2, 3, 4]

    console.log(arr);   // output => [ 1, 2, 3, 4]

In above case,

- Am passing reference of array to the function called add_item.

- Then, pushing value of 4 into the local variable.

- By consoling both the values locally and globally it shows that, changes made to the array inside the function updated the array outside.

Example 2 -:

    let obj = { name: "Nandy", age: 26 };

    let copyOfObj = obj;

    console.log(obj === copyOfObj); // output => true

    copyOfObj["name"] = "Kumar";

    console.log(copyOfObj);         // output => { name: 'Kumar', age: 26 }
    console.log(obj);               // output => { name: 'Kumar', age: 26 }

In above case,

- Am copying the reference of obj to the variable copyOfObj.

- Then, comparing the objects of both variables returns true.

- while changing the name of copyOfObj

- By consoling both the objescts it shows that, changes made to the copyOfObj affecting parent obj.

> **Reference and Resources**

- [Geeks for Geeks](https://www.geeksforgeeks.org/call-by-value-vs-call-by-reference-in-javascript/)

- [Medium](https://medium.com/swlh/call-by-value-call-by-reference-in-javascript-c5738600d9cd)

- [oreilly](https://www.oreilly.com/library/view/javascript-the-definitive/0596000480/ch04s04.html)
