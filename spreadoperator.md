### Spread Operator

- The Spread Operator is used to unpack an iterable (e.g. an array, object, strings etc.) into individual elements.

**_Spread Operator with Arrays_**

**Unpacking arr1 into arr2**

- unpacking an array into another array using spread operator

  let arr1 = [2, 3];
  let arr2 = [1, ...arr1, 4];

  console.log(arr2);

**Output**

    [1,2,3,4]

**_Creating a Copy with array_**

- Spread operator allow us to copy the array into another array, by unpacking the array

        let arr1 = [2, 3];
        let arr2 = [...arr1];

        console.log(arr2);

**Output**

    [2, 3]

**Concatenation**

- To concatenate the morethan one array we can use spread operator.

        let arr1 = [2, 3];
        let arr2 = [4, 5];

        let arr3 = [...arr1, ...arr2];

        console.log(arr3);

**Output**

    [2, 3, 4, 5]

**Spread Operator with Objects**

- Inside the curly bracket we can unpack the object using spread operator

        let person = { name: "Nandy", age: 26 };

        let personDetails = { ...person, city: "Tamilnadu" };

        console.log(personDetails);

**Output**

     {name: "Nandy", age: 26, city: "Tamilnadu"}

**Creating a Copy of Objects**

- Spread operator allow us to copy the object property and value into another object

        let person = { name: "Nandy", age: 26 };
        let personDetails = { ...person };

        console.log(personDetails);

**Output**

    {name: "Nandy", age: 26}

**Concatenation**

- To concatenate the morethan one objects we can use spread operator.

        let person = { name: "Nandy", age: 26 };
        let address = { city: "Tamilnadu", pincode: 627401 };

        let personDetails = { ...person, ...address };

        console.log(personDetails);

**Output**

    {name: "Nandy", age: 26, city: "Tamilnadu", pincode: 627401}

**Spread operator with Strings**

- Using spread operator we can unpack string into an array

        const myName = "Nandy";

        const newArr = [...myName];

        console.log(newArr);

**Output**

    [ 'N', 'a', 'n', 'd', 'y' ];

**Spread Operator with Function Calls**

- Using spread operator we can unpack the arrays and send it to a multiple valuess

        function add(a, b, c) {
            return a + b + c;
        }
        let numbers = [1, 2, 3, 4, 5];

        console.log(add(...numbers));

**Output**

    6

- The Spread Operator syntax can be used to pass an array of arguments to the function.
- Extra values will be ignored if we pass more arguments than the function parameters.

**Resources and Reference**

- [Geeks for Geeks](https://www.geeksforgeeks.org/javascript-spread-operator/)

- [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)

- [W3schools](https://www.w3schools.com/react/react_es6_spread.asp)
